package com.packtpub.libgdx.canyonbunny.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2; // TexturePacker2 нет в новых реализациях
import com.badlogic.gdx.tools.imagepacker.TexturePacker2.Settings;
import com.packtpub.libgdx.canyonbunny.CanyonBunnyMain;

public class DesktopLauncher {

    private static boolean rebuildAtlas = true;
    private static boolean drawDebugOutline = true;

	public static void main (String[] arg) {
        if (rebuildAtlas) {
            Settings settings = new Settings();
            settings.maxWidth = 1024;
            settings.maxHeight = 1024;
            settings.debug = drawDebugOutline;
            TexturePacker2.process(settings, "assets-raw/images", "assets/images", "canyonbunny.pack");
            // android/assets/assets
            // ../CanyonBunny-android/assets/images
            // "assets-raw/images", "assets/images", "canyonbunny.pack"
            // Asset not loaded: assets/images/canyonbunny.pack
            TexturePacker2.process(settings, "assets-raw/images-ui", "assets/images", "canyonbunny-ui.pack");
        }

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 800;
        config.height = 480;
		new LwjglApplication(new CanyonBunnyMain(), config);
	}
}
